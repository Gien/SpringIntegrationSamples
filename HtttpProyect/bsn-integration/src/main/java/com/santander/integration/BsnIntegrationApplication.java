package com.santander.integration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource({"classpath:http-inbound-adapter.xml"})
public class BsnIntegrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(BsnIntegrationApplication.class, args);
	}
}
