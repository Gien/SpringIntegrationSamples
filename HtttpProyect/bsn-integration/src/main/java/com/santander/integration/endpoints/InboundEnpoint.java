package com.santander.integration.endpoints;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

@Component
public class InboundEnpoint {
	private Logger log = LoggerFactory.getLogger(this.getClass().getName());
	
	public  Message<?>  getAny(Message<?>msg){
		UUID id = UUID.randomUUID();
		log.info("get methodAny {}: {}", id, msg.getPayload().toString());
		String mensaje = msg.getPayload().toString();
		return MessageBuilder.withPayload("Hola "+mensaje+" !!").copyHeadersIfAbsent(msg.getHeaders())
				.setHeader("http_statusCode", HttpStatus.OK).build();
	}
}

