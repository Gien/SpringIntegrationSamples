package com.santander.integration.gateways;

import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DelCreditCardEcho {
	private static Log logger = LogFactory.getLog(DelCreditCardEcho.class);

	public Source issueResponseFor(DOMSource request) {
		return request;
	}
}
