package com.santander.integration.types;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "set-credit-cards")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"creditcards"})
public class SetCreditCards {
    @XmlElement(required = true)
    protected List<SetCreditCard> creditcards;
    /**
     * Obtiene el valor de la propiedad type.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public List<SetCreditCard> getCreditCard() {
        return creditcards;
    }
    /**
     * Define el valor de la propiedad type.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditCard(List<SetCreditCard> value) {
        this.creditcards = value;
    }

}
