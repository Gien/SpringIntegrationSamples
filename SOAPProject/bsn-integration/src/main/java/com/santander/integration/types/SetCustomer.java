package com.santander.integration.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "set-customer")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"name", "identificator", "phone"})
public class SetCustomer {

	private String name;

	private String identificator;

	private int phone;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getIdentificator() {
		return identificator;
	}

	public void setIdentificator(String identificator) {
		this.identificator = identificator;
	}

	public int getPhone() {
		return phone;
	}

	public void setPhone(int phone) {
		this.phone = phone;
	}

	@Override
	public String toString(){
		return "[Customer (name="+this.name+", identificator="+this.identificator+", phone="+this.phone+")]";
	}
	
}

