package com.santander.integration.domain.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.santander.integration.domain.entities.CustomerEntity;

@Repository
public interface CustomerRepository extends JpaRepository<CustomerEntity, Integer> {
	
	@Query("SELECT t FROM CustomerEntity t where t.name = :name") 
	public CustomerEntity findCustomerEntityByName(@Param("name")String name);
	
	@Query("SELECT t FROM CustomerEntity t where t.phone = :phone") 
	public List<CustomerEntity> findCustomerEntityByPhone(@Param("phone")int phone);

	@Query("SELECT t FROM CustomerEntity t where t.identificator = :identificator") 
	public CustomerEntity findCustomerEntityByIdentificator(@Param("identificator")String identificator);

	@Query("SELECT t FROM CustomerEntity t where t.identificator = :identificator or t.phone = :phone") 
	public List<CustomerEntity> findCustomerEntityByIdentificatorAndPhone(@Param("identificator")String identificator, @Param("phone")int phone);

	@Query("SELECT count(t) FROM CustomerEntity t where t.identificator = :identificator") 
	public int countCustomerEntityByIdentificator(@Param("identificator")String identificator);
	
	@Query("DELETE FROM CustomerEntity t where t.identificator = :identificator") 
	public void deleteCustomerEntityByIdentificator(@Param("identificator")String identificator);
}
