package com.santander.integration.service.impl;

import javax.xml.transform.Source;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.xml.source.DomSourceFactory;
import org.springframework.stereotype.Component;

import com.santander.integration.domain.entities.CustomerEntity;
import com.santander.integration.domain.repositories.CustomerRepository;
import com.santander.integration.service.ICustomerService;
import com.santander.integration.types.PutCustomer;
import com.santander.integration.types.SetCustomer;

@Component
public class CustomerService implements ICustomerService{
	private static Log logger = LogFactory.getLog(CustomerService.class);
	
	@Autowired
	CustomerRepository customerRepository;
	
	@Override
	public Source addCustomer(SetCustomer customer){
		try{
			logger.info("Add = " + customer.toString());
			customerRepository.save(setCutomerbyXMLCustomer(customer));
		
			return generateSource("Add Customer is OK");
		}catch(Exception e){
			return generateSource("Error: "+e.getMessage());
		}
	}
	
	private CustomerEntity setCutomerbyXMLCustomer(SetCustomer customerXML){
		CustomerEntity customer = new CustomerEntity();
		customer.setName(customerXML.getName());
		customer.setIdentificator(customerXML.getIdentificator());
		customer.setPhone(customerXML.getPhone());
		return customer;
	}
	
	@Override
	public Source putCustomer(PutCustomer customer){
		try{
			logger.info("Put = " + customer.toString());
			customerRepository.save(setCutomerbyXMLCustomer(customer));
			
			return generateSource("Update Customer is OK");
		}catch(Exception e){
			return generateSource("Error: "+e.getMessage());
		}
	}
	
	private CustomerEntity setCutomerbyXMLCustomer(PutCustomer customerXML){
		CustomerEntity customer = customerRepository.findOne(customerXML.getId());
		customer.setName(customerXML.getName());
		customer.setIdentificator(customerXML.getIdentificator());
		customer.setPhone(customerXML.getPhone());
		return customer;
	}
	
	@Override
	public CustomerEntity getCustomerByIdentificator(String identificator){
		return customerRepository.findCustomerEntityByIdentificator(identificator);
	}
	
	private Source generateSource(String message){
		return new DomSourceFactory().createSource(
				"<echoResponse xmlns=\"http://www.springframework.org/spring-ws/samples/echo\">"+message+"</echoResponse>");
	}

}
