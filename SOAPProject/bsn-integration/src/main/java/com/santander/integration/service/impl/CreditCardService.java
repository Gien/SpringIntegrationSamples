package com.santander.integration.service.impl;

import javax.xml.transform.Source;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.xml.source.DomSourceFactory;

import com.santander.integration.domain.entities.CreditCardEntity;
import com.santander.integration.domain.repositories.CreditCardRepository;
import com.santander.integration.service.ICustomerService;
import com.santander.integration.types.DelCreditCard;
import com.santander.integration.types.SetCreditCard;
import com.santander.integration.types.SetCreditCards;

public class CreditCardService {
	private static Log logger = LogFactory.getLog(CreditCardService.class);
	
	@Autowired
	CreditCardRepository creditCardRepository;
	
	@Autowired
	ICustomerService customerService;
	
	public Source addCreditCards(SetCreditCards creditCardsXML){
		try{
			logger.info("Add Credit Card: " + creditCardsXML.toString());
			String result = "<identificators>";
			for (SetCreditCard creditCardXML : creditCardsXML.getCreditCard()){
				result += "<id>"+creditCardRepository.save(setCreditCardByXML(creditCardXML)).getId()+"</id>";
			}
			result += "</identificators>";
			return generateSource(result);
		}catch(Exception e){
			return generateSource("Error: "+e.getMessage());
		}
	}
	
	private CreditCardEntity setCreditCardByXML(SetCreditCard creditCardXML){
		CreditCardEntity creditCard = new CreditCardEntity();
		creditCard.setCustomer(customerService.getCustomerByIdentificator(creditCardXML.getIdentificator()));
		creditCard.setType(creditCardXML.getType());
		return creditCard;
	}
	
	public Source delCreditCard(DelCreditCard creditCardXML){
		try{
			logger.info("del Credit Card: " + creditCardXML.toString());
			CreditCardEntity creditCard = creditCardRepository.findOne(creditCardXML.getId());
			int tsTime = (int) (System.currentTimeMillis()/1000);
			creditCard.setTsDelete(tsTime);
			creditCardRepository.save(creditCard);
			return generateSource("Delete Credit Cart is OK");
		}catch(Exception e){
			return generateSource("Error: "+e.getMessage());
		}
	}
	
	private Source generateSource(String message){
		return new DomSourceFactory().createSource(
				"<echoResponse xmlns=\"http://www.springframework.org/spring-ws/samples/echo\">"+message+"</echoResponse>");
	}
}
