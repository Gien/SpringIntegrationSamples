package com.santander.integration.gateways;

import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class PutCustomerEcho {
	private static Log logger = LogFactory.getLog(PutCustomerEcho.class);

	public Source issueResponseFor(DOMSource request) {

//		Node nodeList = request.getNode();
//		logger.info("Node name: "+nodeList.getNodeName());
//		
//		JAXBContext jaxbContext = JAXBContext.newInstance(Customer.class);
//		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
//		Customer customer1 = (Customer) unmarshaller.unmarshal(request);
		return request;
//		return new DomSourceFactory().createSource(
//				"<echoResponse xmlns=\"http://www.springframework.org/spring-ws/samples/echo\">" +
//				request.getNode().getTextContent() + "</echoResponse>");
	}

}
