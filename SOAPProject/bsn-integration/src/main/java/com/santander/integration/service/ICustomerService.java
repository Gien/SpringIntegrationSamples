package com.santander.integration.service;

import javax.xml.transform.Source;

import com.santander.integration.domain.entities.CustomerEntity;
import com.santander.integration.types.PutCustomer;
import com.santander.integration.types.SetCustomer;

public interface ICustomerService {

	Source addCustomer(SetCustomer customer);

	Source putCustomer(PutCustomer customer);

	CustomerEntity getCustomerByIdentificator(String identificator);

}
