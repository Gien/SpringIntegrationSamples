package com.santander.integration.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlRootElement(name = "set-credit-card")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"type", "identificator"})
public class SetCreditCard {
    @XmlElement(required = true)
    protected String type;
    @XmlElement(required = true)
    protected String identificator;
    /**
     * Obtiene el valor de la propiedad type.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }
    /**
     * Define el valor de la propiedad type.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }
    /**
     * Obtiene el valor de la propiedad identificator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificator() {
        return identificator;
    }
    /**
     * Define el valor de la propiedad identificator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificator(String value) {
        this.identificator = value;
    }

	@Override
	public String toString(){
		return "[Set Credit Card (type="+this.type+", identificator="+this.identificator+")]";
	}
}
