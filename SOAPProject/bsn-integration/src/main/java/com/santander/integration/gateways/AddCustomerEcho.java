package com.santander.integration.gateways;

import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class AddCustomerEcho {
	private static Log logger = LogFactory.getLog(AddCustomerEcho.class);

	public Source issueResponseFor(DOMSource request) {
		return request;
	}

}
