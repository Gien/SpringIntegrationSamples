package com.santander.integration;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.santander.integration.gateway.CustomerGateway;

@SpringBootApplication
public class BsnIntegrationApplication {
	private static Log logger = LogFactory.getLog(BsnIntegrationApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(BsnIntegrationApplication.class, args);
		ApplicationContext context = new ClassPathXmlApplicationContext("classpath:xml-transform-to-customer.xml");
		final CustomerGateway service = context.getBean(CustomerGateway.class);
		StringBuffer sXML = new StringBuffer("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
		sXML.append("<customer>");
		sXML.append("    <name>Nombre Prueba</name>");
		sXML.append("    <surname>Apellidos Prueba</surname>");
		sXML.append("</customer>");
		
		logger.info("XML = " + sXML.toString());

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();  
        DocumentBuilder builder; 
        Document doc;
        try {
            builder = factory.newDocumentBuilder();  
			doc = builder.parse( new InputSource( new StringReader( sXML.toString() ) ) );
			service.send(doc);
		} catch (SAXException | IOException | ParserConfigurationException e) {
			logger.error(e);
		} 
	}
}
