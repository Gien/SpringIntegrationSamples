package com.santander.integration.transform;

import java.io.StringWriter;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.w3c.dom.Document;

import com.santander.integration.BsnIntegrationApplication;

public class TransformerToXML {
	private static Log logger = LogFactory.getLog(BsnIntegrationApplication.class);
	public Message<?> inboundTransformerMethodMailling(Document doc) throws TransformerException{
		logger.info("TransformerToXML.inboundTransformerMethodMailling.contenido: " +toXmlString(doc));
		return MessageBuilder.withPayload(toXmlString(doc)).build();
	}
	
	private static String toXmlString(Document document) throws TransformerException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(document);
        StringWriter strWriter = new StringWriter();
        StreamResult result = new StreamResult(strWriter);
    
        transformer.transform(source, result);
        
        return strWriter.getBuffer().toString();
        
    }
}
