package com.santander.integration.gateway;

import org.springframework.messaging.Message;
import org.w3c.dom.Document;

import com.santander.integration.domain.Customer;

public interface CustomerGateway {

	public Message<Customer> send(Document doc);
}
