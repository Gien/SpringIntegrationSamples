package com.santander.integration;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import com.santander.integration.domain.Customer;

public class Prueba {
    
    private static final String BOOKSTORE_XML = "./customer -jaxb.xml";
	public Prueba() throws JAXBException, FileNotFoundException{
    	
    	Customer customer = new Customer();
    	
    	customer.setName("Probando");
    	customer.setSurname("XML Prueba");
    	
    	// create JAXB context and instantiate marshaller
        JAXBContext context2 = JAXBContext.newInstance(Customer.class);
        Marshaller m = context2.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

        // Write to System.out
        m.marshal(customer, System.out);

        // Write to File
        m.marshal(customer, new File(BOOKSTORE_XML));
        System.out.println(m.toString());

        // get variables from our xml file, created before
        System.out.println();
        System.out.println("Output from our XML File: ");
        Unmarshaller um = context2.createUnmarshaller();
        Customer customer2 = (Customer) um.unmarshal(new FileReader(
                BOOKSTORE_XML));
        
    }
}
