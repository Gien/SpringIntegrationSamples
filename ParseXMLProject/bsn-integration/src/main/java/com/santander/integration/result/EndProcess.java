package com.santander.integration.result;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.santander.integration.BsnIntegrationApplication;
import com.santander.integration.domain.Customer;

public class EndProcess {
	private static Log logger = LogFactory.getLog(EndProcess.class);
	
	public void endMessage(Customer customer){
		logger.info("Resultado = " + customer.toString());
	}
}
