package com.santander.integration.domain;

import java.io.IOException;

import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonRootName(value = "Customer")
public class Customer {

    @JsonProperty("name")
	String name;
    @JsonProperty("surname")
	String surname;
 
    public Customer() {
        super();
    }
 
    public Customer(String args) throws JsonParseException, JsonMappingException, IOException {

    	if (args.contains("{")){
    		args = args.substring(1,args.length()-1);
    	}
    	
    	String[] pairs = args.split(",");
    	
		for (int i=0;i<pairs.length;i++) {
		    String pair = pairs[i].trim();
		    String[] keyValue = pair.split(":");
		    if (keyValue[0].equals("name")){
		    	name = keyValue[1];
		    }
		    if (keyValue[0].equals("surname")){
		    	surname = keyValue[1];
		    }
		}
    }
    
    public Customer(String name, String surname){
		this.name = name;
		this.surname = surname;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	@JsonCreator
	public static Module Create(String jsonString) throws JsonParseException, JsonMappingException, IOException {
	    ObjectMapper mapper = new ObjectMapper();
	    Module module = null;
	    module = mapper.readValue(jsonString, Module.class);
	    return module;
	}
	
	@Override
	public String toString(){
		return "Customer = [name = " + this.name + " & surname = " + this.surname + "]";  
	}
	
	public void toMessage(){
	}
}
