package com.santander.integration.gateway;

import org.springframework.messaging.Message;

import com.santander.integration.domain.Customer;

public interface CustomerGateway {

	public Message<Customer> send(Message<?> msg);
}
