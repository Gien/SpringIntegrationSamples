package com.santander.integration;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.integration.support.MessageBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.santander.integration.gateway.CustomerGateway;

@SpringBootApplication
public class BsnIntegrationApplication {
	private static Log logger = LogFactory.getLog(BsnIntegrationApplication.class);

	public static void main(String[] args) throws JsonProcessingException {
		SpringApplication.run(BsnIntegrationApplication.class, args);
		
		
		ApplicationContext context = new ClassPathXmlApplicationContext("classpath:json-transform-to-customer.xml");
		final CustomerGateway service = context.getBean(CustomerGateway.class);
		
		Map<String, Object> clientMap = new HashMap<>();
		clientMap.put("name","Nombre" );
		clientMap.put("surname", "Apellidos");
		
		String json = new ObjectMapper().writeValueAsString(clientMap);
		
		service.send(MessageBuilder.withPayload(json.toString()).build()).getPayload();
	}
}
