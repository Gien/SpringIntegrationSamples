package com.santander.integration.config;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.ftpserver.FtpServer;
import org.apache.ftpserver.FtpServerFactory;
import org.apache.ftpserver.ftplet.FtpException;
import org.apache.ftpserver.ftplet.UserManager;
import org.apache.ftpserver.listener.ListenerFactory;
import org.apache.ftpserver.usermanager.PropertiesUserManagerFactory;
import org.apache.ftpserver.usermanager.impl.BaseUser;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;

public class FTPConfiguration {
	private static Log log = LogFactory.getLog(FTPConfiguration.class);
	
	public static final String REMOTE_HOST = "localhost";
	public static final int REMOTE_PORT = 2221;
	public static final String USERNAME = "username";
	public static final String PASSWORD = "password";
	public static final String DIRECTORY = "/tmp";
	public static final String LOCAL_DIRECTORY = "/procesado";
	
	public FTPConfiguration(){
		createFtpServer();
		
		log.info("Cargando Bean.....");
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
                "classpath:ftp-inbound-adapter.xml");
	}

	private void createFtpServer(){
		log.info("Generando Servidor FTP....");
		PropertiesUserManagerFactory userManagerFactory = new PropertiesUserManagerFactory();
		UserManager userManager = userManagerFactory.createUserManager();
		BaseUser user = new BaseUser();
		user.setName(USERNAME);
		user.setPassword(PASSWORD);
		user.setHomeDirectory(DIRECTORY);
		try {
			userManager.save(user);
		} catch (FtpException e) {
			log.error(e);
		}
		ListenerFactory listenerFactory = new ListenerFactory();
		listenerFactory.setPort(REMOTE_PORT);
	   
		FtpServerFactory factory = new FtpServerFactory();
		factory.setUserManager(userManager);
		factory.addListener("default", listenerFactory.createListener());
	   
		FtpServer server = factory.createServer();
		try {
			server.start();
			log.info("Arrancado Servidor FTP....");
		} catch (FtpException e) {
			log.error(e);
		}
	}
}

