package com.santander.integration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

import com.santander.integration.config.FTPConfiguration;

@SpringBootApplication
public class BsnIntegrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(BsnIntegrationApplication.class, args);
    	FTPConfiguration ftp = new FTPConfiguration();

	}
}
