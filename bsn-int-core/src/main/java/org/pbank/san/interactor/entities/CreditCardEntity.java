package org.pbank.san.interactor.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="t_credit_card")
public class CreditCardEntity extends BaseEntity<Integer> implements Serializable {
	
	@Column(name="tipo")
	private String type;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "dni_customer")
	private CustomerEntity customer;
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public CustomerEntity getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerEntity customer) {
		this.customer = customer;
	}

}
