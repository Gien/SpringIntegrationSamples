//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2018.01.31 a las 11:08:26 AM CET 
//


package io.spring.guides.gs_producing_web_service;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the io.spring.guides.gs_producing_web_service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: io.spring.guides.gs_producing_web_service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetCustomerResponse }
     * 
     */
    public GetCustomerResponse createGetCustomerResponse() {
        return new GetCustomerResponse();
    }

    /**
     * Create an instance of {@link Customer }
     * 
     */
    public Customer createCustomer() {
        return new Customer();
    }

    /**
     * Create an instance of {@link SetCreditCardsRequest }
     * 
     */
    public SetCreditCardsRequest createSetCreditCardsRequest() {
        return new SetCreditCardsRequest();
    }

    /**
     * Create an instance of {@link Creditcard }
     * 
     */
    public Creditcard createCreditcard() {
        return new Creditcard();
    }

    /**
     * Create an instance of {@link GetCustomerRequest }
     * 
     */
    public GetCustomerRequest createGetCustomerRequest() {
        return new GetCustomerRequest();
    }

    /**
     * Create an instance of {@link SetCreditCardsResponse }
     * 
     */
    public SetCreditCardsResponse createSetCreditCardsResponse() {
        return new SetCreditCardsResponse();
    }

    /**
     * Create an instance of {@link SetCustomerRequest }
     * 
     */
    public SetCustomerRequest createSetCustomerRequest() {
        return new SetCustomerRequest();
    }

    /**
     * Create an instance of {@link ResultResponse }
     * 
     */
    public ResultResponse createResultResponse() {
        return new ResultResponse();
    }

    /**
     * Create an instance of {@link DelCreditCardsRequest }
     * 
     */
    public DelCreditCardsRequest createDelCreditCardsRequest() {
        return new DelCreditCardsRequest();
    }

    /**
     * Create an instance of {@link PutCustomerRequest }
     * 
     */
    public PutCustomerRequest createPutCustomerRequest() {
        return new PutCustomerRequest();
    }

}
