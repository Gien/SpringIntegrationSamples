package integrator.endpoints;

import java.util.ArrayList;
import java.util.List;

import org.pbank.san.interactor.entities.CreditCardEntity;
import org.pbank.san.interactor.entities.CustomerEntity;
import org.pbank.san.interactor.repositories.CreditCardRepository;
import org.pbank.san.interactor.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import io.spring.guides.gs_producing_web_service.Creditcard;
import io.spring.guides.gs_producing_web_service.Customer;
import io.spring.guides.gs_producing_web_service.DelCreditCardsRequest;
import io.spring.guides.gs_producing_web_service.GetCustomerRequest;
import io.spring.guides.gs_producing_web_service.GetCustomerResponse;
import io.spring.guides.gs_producing_web_service.PutCustomerRequest;
import io.spring.guides.gs_producing_web_service.ResultResponse;
import io.spring.guides.gs_producing_web_service.SetCreditCardsRequest;
import io.spring.guides.gs_producing_web_service.SetCreditCardsResponse;
import io.spring.guides.gs_producing_web_service.SetCustomerRequest;

@Endpoint
public class CustomerEndpoint {
	private static final String NAMESPACE_URI = "http://spring.io/guides/gs-producing-web-service";
	
	@Autowired
	private CustomerRepository customerRepo;
	
	@Autowired
	private CreditCardRepository creditCardRepo;

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getCustomerRequest")
	@ResponsePayload
	public GetCustomerResponse getCustomerRequest(@RequestPayload GetCustomerRequest request) throws Exception {
		GetCustomerResponse response = new GetCustomerResponse();
		response.setCustomer(setCustomerXML(customerRepo.findCustomerEntityByIdentificator(request.getIdentificator())));

		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "setCustomerRequest")
	@ResponsePayload
	public ResultResponse setCustomerRequest(@RequestPayload SetCustomerRequest request) throws Exception {
		customerRepo.save(getCustomerByXML(new CustomerEntity(), setAddCustomerXML(request)));
		return getReponse("Generate Customer is Ok");
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "putCustomerRequest")
	@ResponsePayload
	public ResultResponse putCustomerRequest(@RequestPayload PutCustomerRequest request) throws Exception {
		customerRepo.save(getCustomerByXML(customerRepo.findOne(request.getCustomer().getId()), request.getCustomer()));
		return getReponse("Update Customer is OK");
	}

	private ResultResponse getReponse(String message) {
		ResultResponse result = new ResultResponse();
		result.setResult(message);
		return result;
	}
	
	private Customer setAddCustomerXML(SetCustomerRequest customer){
		Customer customerXML = new Customer();
		customerXML.setName(customer.getName());
		customerXML.setIdentificator(customer.getIdentificator());
		customerXML.setPhone(customer.getPhone());
		return customerXML;
	}
	
	private Customer setCustomerXML(CustomerEntity customer) throws Exception{
		if (customer == null ) {
			throw throwException("Client not found.");
		}
		Customer customerXML = new Customer();
		customerXML.setId(customer.getId());
		customerXML.setName(customer.getName());
		customerXML.setIdentificator(customer.getIdentificator());
		customerXML.setPhone(customer.getPhone());
		return customerXML;
	}
	
	private CustomerEntity getCustomerByXML(CustomerEntity customerEntity, Customer customer) throws Exception{
		validateCustomerXML(customer);
		customerEntity.setName(customer.getName());
		customerEntity.setIdentificator(customer.getIdentificator());
		customerEntity.setPhone(customer.getPhone());
		return customerEntity;
	}

	private void validateCustomerXML(Customer customer) throws Exception {
		if (StringUtils.isEmpty(customer.getName())){ throw throwException("Name is requiered");}
		if (customer.getName().length()> 24) { throw throwException("Name size 24");}
		if (StringUtils.isEmpty(customer.getIdentificator())){ throw throwException("Identificator is requiered");}
		if (customer.getIdentificator().length()> 24) { throw throwException("Identificator size 24");}
		if (StringUtils.isEmpty(String.valueOf(customer.getPhone()))){ throw throwException("Phone is requiered");}
		if (String.valueOf(customer.getPhone()).length()> 9) { throw throwException("Phone size 9");}
	}
	
	private Exception throwException(String message){
		return new Exception(message);
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "setCreditCardsRequest")
	@ResponsePayload
	public SetCreditCardsResponse setCreditCardsResponse(@RequestPayload SetCreditCardsRequest request) throws Exception {
		SetCreditCardsResponse result = new SetCreditCardsResponse();
		for (CreditCardEntity creditCard: setCreditCartByXML(request)){
			result.setCreditcardId(Integer.valueOf(creditCardRepo.save(creditCard).getId()));
		}
		return result;
	}
	
	public List<CreditCardEntity> setCreditCartByXML(SetCreditCardsRequest request) throws Exception{
		List<CreditCardEntity> creditCards = new ArrayList<>();
		for (Creditcard creditCardXML : request.getCreditCards()){
			if (customerRepo.countCustomerEntityByIdentificator(creditCardXML.getIdentificator())>0){
				CreditCardEntity creditCard = new CreditCardEntity();
				validateCreditCardXML(creditCardXML);
				creditCard.setType(creditCardXML.getType());
				creditCard.setCustomer(customerRepo.findCustomerEntityByIdentificator(creditCardXML.getIdentificator()));
				creditCards.add(creditCard);
			}
		}
		return creditCards;
	}
	
	private void validateCreditCardXML(Creditcard creditCardXML) throws Exception{
		if (StringUtils.isEmpty(creditCardXML.getType())){ throw throwException("Type credit card is requiered");}
		if (creditCardXML.getType().length()> 15) { throw throwException("Type credit card size 15");}
		if (StringUtils.isEmpty(creditCardXML.getIdentificator())){ throw throwException("Identificator credit card is requiered");}
		if (creditCardXML.getIdentificator().length()> 24) { throw throwException("Identificator credit card size 24");}
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "delCreditCardsRequest")
	@ResponsePayload
	public ResultResponse delCreditCardsRequest(@RequestPayload DelCreditCardsRequest request) throws Exception {
		if (!creditCardRepo.exists(request.getCreditcardId())){
			throw throwException("Credit card not found");
		}
		creditCardRepo.delete(request.getCreditcardId());
		return getReponse("Delete credit card is OK");
	}
}


