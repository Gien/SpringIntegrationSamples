/*
 * Copyright 2014-2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package integrator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.ClassUtils;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.client.SoapFaultClientException;

import io.spring.guides.gs_producing_web_service.Creditcard;
import io.spring.guides.gs_producing_web_service.Customer;
import io.spring.guides.gs_producing_web_service.DelCreditCardsRequest;
import io.spring.guides.gs_producing_web_service.GetCustomerRequest;
import io.spring.guides.gs_producing_web_service.GetCustomerResponse;
import io.spring.guides.gs_producing_web_service.PutCustomerRequest;
import io.spring.guides.gs_producing_web_service.ResultResponse;
import io.spring.guides.gs_producing_web_service.SetCreditCardsRequest;
import io.spring.guides.gs_producing_web_service.SetCreditCardsResponse;
import io.spring.guides.gs_producing_web_service.SetCustomerRequest;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ApplicationIntegrationTests {
	
	private String TEXTO = "";
	private static final Integer INTEGER = 666666666;

    private Jaxb2Marshaller marshaller = new Jaxb2Marshaller();

    @LocalServerPort
    private int port = 0;

    @Before
    public void init() throws Exception {        
        marshaller.setPackagesToScan(ClassUtils.getPackageName(SetCustomerRequest.class));
        marshaller.setPackagesToScan(ClassUtils.getPackageName(GetCustomerRequest.class));
        marshaller.setPackagesToScan(ClassUtils.getPackageName(PutCustomerRequest.class));
        marshaller.afterPropertiesSet();
    }

    @Test
    public void SetCustomerRequest() {
    	TEXTO = UUID.randomUUID().toString().substring(0,10);
    	ResultResponse response = new ResultResponse();

        //Insert
    	WebServiceTemplate ws = new WebServiceTemplate(marshaller);
        SetCustomerRequest request = new SetCustomerRequest();
        request.setName(TEXTO);
        request.setIdentificator(TEXTO);
        request.setPhone(INTEGER);

        ResultResponse responseCustomer = (ResultResponse) ws.marshalSendAndReceive("http://localhost:" + port + "/ws", request);
        assertTrue(responseCustomer.getResult().equals("Generate Customer is Ok"));
        
        //Query
        WebServiceTemplate wsQuery = new WebServiceTemplate(marshaller);
        GetCustomerRequest requestQuery = new GetCustomerRequest();
        requestQuery.setIdentificator(TEXTO);
        GetCustomerResponse responseQuery = (GetCustomerResponse) wsQuery.marshalSendAndReceive("http://localhost:"
                + port + "/ws", requestQuery);
        assertThat(responseQuery.getCustomer().getId()).isNotNull();
        
        //Update
        WebServiceTemplate wsQueryPut = new WebServiceTemplate(marshaller);
        PutCustomerRequest requestQueryPut = new PutCustomerRequest();
        
        requestQueryPut.setCustomer(responseQuery.getCustomer());
        responseCustomer = (ResultResponse) wsQueryPut.marshalSendAndReceive("http://localhost:" + port + "/ws", requestQueryPut);
        	
        assertTrue(responseCustomer.getResult().equals("Update Customer is OK"));
        
        //UpdateThrow
        WebServiceTemplate wsQueryThrow = new WebServiceTemplate(marshaller);
        PutCustomerRequest requestQueryThrow = new PutCustomerRequest();
        
        Customer customer = responseQuery.getCustomer();
        customer.setName(null);
        
        requestQueryThrow.setCustomer(customer);
        try{
        	wsQueryThrow.marshalSendAndReceive("http://localhost:"
	                + port + "/ws", requestQueryThrow);
        }catch(SoapFaultClientException e){
        	assertThat(e).isNotNull();
        }
        
        WebServiceTemplate wsAddCreditCard = new WebServiceTemplate(marshaller);
        SetCreditCardsRequest requestAddCreditCard = new SetCreditCardsRequest();
        List<Creditcard> creditCards = new ArrayList<>();
        Creditcard creditcard = new Creditcard();
        creditcard.setIdentificator(TEXTO);
        creditcard.setType(TEXTO);
        creditCards.add(creditcard);
        requestAddCreditCard.setCreditCards(creditCards);
        SetCreditCardsResponse responseCreditCard = (SetCreditCardsResponse) wsAddCreditCard.marshalSendAndReceive("http://localhost:" + port + "/ws", requestAddCreditCard);
        assertThat(responseCreditCard).isNotNull();
        
        WebServiceTemplate wsAddCreditCardThrow = new WebServiceTemplate(marshaller);
        SetCreditCardsRequest requestAddCreditCardThrow = new SetCreditCardsRequest();
        List<Creditcard> creditCardsThrow = new ArrayList<>();
        Creditcard creditcardThrow = new Creditcard();
        creditcardThrow.setIdentificator(TEXTO);
        creditcardThrow.setType(null);
        creditCardsThrow.add(creditcardThrow);
        requestAddCreditCardThrow.setCreditCards(creditCardsThrow);
        try{
	        wsAddCreditCardThrow.marshalSendAndReceive("http://localhost:" + port + "/ws", requestAddCreditCardThrow);
	    }catch(SoapFaultClientException e){
	    	assertThat(e).isNotNull();
	    }

        WebServiceTemplate wsDelCreditCard = new WebServiceTemplate(marshaller);
        DelCreditCardsRequest requestdelCreditCard = new DelCreditCardsRequest();
        requestdelCreditCard.setCreditcardId(responseCreditCard.getCreditcardId().get(0));
        
        ResultResponse responseAddCreditCard = (ResultResponse) wsDelCreditCard.marshalSendAndReceive("http://localhost:" + port + "/ws", requestdelCreditCard);
        assertTrue(responseAddCreditCard.getResult().equals("Delete credit card is OK"));
        
        try{
        	wsDelCreditCard.marshalSendAndReceive("http://localhost:" + port + "/ws", requestdelCreditCard);
        }catch(SoapFaultClientException e){
        	assertTrue(e.getMessage().equals("Credit card not found"));
        }
    }

    @Test(expected=SoapFaultClientException.class)
    public void SetCustomerRequest_throw() {
        WebServiceTemplate ws = new WebServiceTemplate(marshaller);
        SetCustomerRequest request = new SetCustomerRequest();
        request.setName(null);
        request.setIdentificator(TEXTO);
        request.setPhone(INTEGER);
        ws.marshalSendAndReceive("http://localhost:"
                + port + "/ws", request);
    }
}