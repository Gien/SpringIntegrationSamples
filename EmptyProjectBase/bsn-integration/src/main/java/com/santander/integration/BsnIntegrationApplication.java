package com.santander.integration.bsnintegration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BsnIntegrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(BsnIntegrationApplication.class, args);
	}
}
