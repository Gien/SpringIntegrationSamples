package com.santander.integration.mail;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class GmailInboundImapIdleAdapterTestApp {
	
	private static Log logger = LogFactory.getLog(GmailInboundImapIdleAdapterTestApp.class);

	public void inico () throws Exception {
		@SuppressWarnings("resource")
		ClassPathXmlApplicationContext ac = new ClassPathXmlApplicationContext(
				"classpath:gmail-imap-idle-config.xml");
	}
}

