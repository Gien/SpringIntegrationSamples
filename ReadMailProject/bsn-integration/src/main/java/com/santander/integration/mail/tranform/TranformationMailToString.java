package com.santander.integration.mail.tranform;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;

public class TranformationMailToString {
	private static Log logger = LogFactory.getLog(TranformationMailToString.class);
	
	private static final String BUSCAR_CLIENTE = "El cliente ";
	private static final String BUSCAR_DNI = "con DNI ";
	private static final String BUSCAR_TARJETA = "tarjeta bancaria ";
	
	/**
	 * Method that transform message to inbound
	 * 
	 * @param message the input message to transform.
	 * @return Message transformed of inbound channel.
	 * */
	public Message<?> inboundTransformerMethod(Message<?> message){
		String contenido = getMessageContent(message);
		return MessageBuilder.withPayload(contenido).build();
	}

	private String getMessageContent(Message<?> message) {
		String contenido = message.getPayload().toString();
		if (contenido.contains(BUSCAR_CLIENTE) && contenido.contains(BUSCAR_DNI) && contenido.contains(BUSCAR_TARJETA)){
			int init = contenido.indexOf(BUSCAR_CLIENTE);
			ClienteDTO cliente = getClienteFromString(contenido.substring(init, init +500)
					.replaceAll("=", "")
					.replaceAll("\n", "")
					.replaceAll("\r", ""));
			contenido = "Alta tarjeta con los siguientes datos: [CLIENTE: <"+cliente.getCliente()+">,DNI: <"+cliente.getDni()+">, TIPO DE TARJETA: <" + cliente.getTarjeta()+">]";
			logger.info(contenido);
		}
		return contenido;
	}
	
	private ClienteDTO getClienteFromString(String cuerpoMsg){
		ClienteDTO result = new ClienteDTO();
		result.setCliente(getDatoFromString(cuerpoMsg, BUSCAR_CLIENTE));
		result.setDni(getDatoFromString(cuerpoMsg, BUSCAR_DNI));
		result.setTarjeta(getDatoFromString(cuerpoMsg, BUSCAR_TARJETA));
		return result;
	}
	
	private String getDatoFromString(String input, String cadenaInicio){
		String resultado = "";
		String cadena = input.substring(input.indexOf(cadenaInicio)+cadenaInicio.length());
		for (int i = 0; i<= cadena.length(); i++){
			if (" ".equals(String.valueOf(cadena.charAt(i)))){
				break;
			}
			if (".".equals(String.valueOf(cadena.charAt(i)))){
				break;
			}
			resultado += cadena.charAt(i);
			
		}
		return resultado;
	}

	class ClienteDTO{
		String cliente = "";
		String dni = "";
		String tarjeta = "";
		public String getCliente() {
			return cliente;
		}
		public void setCliente(String cliente) {
			this.cliente = cliente;
		}
		public String getDni() {
			return dni;
		}
		public void setDni(String dni) {
			this.dni = dni;
		}
		public String getTarjeta() {
			return tarjeta;
		}
		public void setTarjeta(String tarjeta) {
			this.tarjeta = tarjeta;
		}
	}
}
