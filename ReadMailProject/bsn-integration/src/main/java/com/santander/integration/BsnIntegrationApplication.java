package com.santander.integration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.santander.integration.mail.GmailInboundImapIdleAdapterTestApp;

@SpringBootApplication
public class BsnIntegrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(BsnIntegrationApplication.class, args);
		
		try {
			new GmailInboundImapIdleAdapterTestApp().inico();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
