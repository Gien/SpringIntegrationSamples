package org.pbank.san.interactor.bsn_int_rest;

import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.pbank.san.interactor.dto.CustomerDTO;
import org.pbank.san.interactor.dto.CustomerUpdateDTO;
import org.pbank.san.interactor.entities.CustomerEntity;
import org.pbank.san.interactor.repositories.CustomerRepository;
import org.pbank.san.interactor.service.customer.impl.CustomerService;

@RunWith(MockitoJUnitRunner.class)
public class CustomerServiceTest {
	
	@InjectMocks
	CustomerService customerServ;
	
	@Mock
	CustomerRepository customerRepository;

	@Test
	public void addCustomer(){
		CustomerDTO customer = new CustomerDTO();
		customer.setIdentificator("Identifi");
		customer.setName("name");
		customer.setPhone(1);
		customerServ.addCustomer(customer);
	}

	@Test
	public void putCustomer(){
		CustomerUpdateDTO customer = new CustomerUpdateDTO();
		customer.setId(1);
		customer.setIdentificator("Identifi");
		customer.setName("name");
		customer.setPhone(1);
		when(customerRepository.getOne(Mockito.anyInt())).thenReturn(new CustomerEntity());
		customerServ.putCustomer(customer);
	}

	@Test
	public void delCustomer() throws Exception{
		CustomerEntity customer = new CustomerEntity();
		when(customerRepository.findCustomerEntityByIdentificator(Mockito.any())).thenReturn(customer);
		customerServ.delCustomer("dni");
	}
}
