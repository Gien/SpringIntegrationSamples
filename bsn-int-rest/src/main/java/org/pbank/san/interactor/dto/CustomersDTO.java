package org.pbank.san.interactor.dto;

import java.util.List;

public class CustomersDTO {
	
	private List<CustomerUpdateDTO> customers;

	public List<CustomerUpdateDTO> getCustomers() {
		return customers;
	}

	public void setCustomers(List<CustomerUpdateDTO> customers) {
		this.customers = customers;
	}

}
