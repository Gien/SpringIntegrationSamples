package org.pbank.san.interactor.service.customer;

import org.pbank.san.interactor.dto.CustomerDTO;
import org.pbank.san.interactor.dto.CustomerQueryDTO;
import org.pbank.san.interactor.dto.CustomerUpdateDTO;
import org.pbank.san.interactor.dto.CustomersDTO;

public interface ICustomerService {

	Void addCustomer(CustomerDTO customer);

	Void putCustomer(CustomerUpdateDTO customer);

	CustomersDTO getCustomer(CustomerQueryDTO customer);

	Void delCustomer(String identificator) throws Exception;

	Void mailCustomer(String identificator, String recipientEmail) throws Exception;

}
