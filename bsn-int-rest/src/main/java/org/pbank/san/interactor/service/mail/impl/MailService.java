package org.pbank.san.interactor.service.mail.impl;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.pbank.san.interactor.service.mail.IMailService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.mysql.jdbc.StringUtils;

//import lombok.extern.slf4j.Slf4j;

@Component
//@Slf4j
public class MailService implements IMailService {
	
	@Value("${server.port}")
	private String MY_SERVER_PORT;

	@Value("${mail.smtp_host}")
	private String smtpHost;
			
	@Value("${mail.smtp_port}")
	private String smtpPort;
	
	@Value("${mail.smtp_mailuser}")
	private String userMail;
	
	@Value("${mail.smtp_mailpass}")
	private String userPass;
	
	private final static String MAIL_SEPARATOR = ";";
	private final static String MAIL_SEPARATOR_BIS = ",";
	
	@Override
	public void setSmtpPort(String smtpPort){
		this.smtpPort = smtpPort;
	}
	
	@Override
	public void setSmtpHost(String smtpHost){
		this.smtpHost = smtpHost;
	}
	
	@Override
	public void setUserMail(String userMail){
		this.userMail = userMail;
	}
	
	@Override
	public void setUserPass(String userPass){
		this.userPass = userPass;
	}
	
	
	public void send(String recipientEmail, String ccEmail, String title, String messagetxt) throws Exception{
        try {
		
	        Properties props = new Properties();
	        props.put("mail.smtp.starttls.enable", "true");
	        props.put("mail.imap.ssl.enable", "true");
	        props.put("mail.smtp.auth", "true");
	        props.put("mail.debug", "true");
	        props.put("mail.smtp.ssl.trust", smtpHost);
	        props.put("mail.smtp.host", smtpHost);
	        props.put("mail.smtp.port", smtpPort);
	
	        Session session = Session.getInstance(props,
	          new javax.mail.Authenticator() {
	            protected PasswordAuthentication getPasswordAuthentication() {
	                return new PasswordAuthentication(userMail, userPass);
	            }
	          });
	        
	        Transport transport = session.getTransport();
            messagetxt = messagetxt.replace("href=\"../", "href=\"https://"+InetAddress.getLocalHost().getHostAddress()+":"+MY_SERVER_PORT+"/")+"<br><br><br><br><br><br>";

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(userMail));

            if (StringUtils.isNullOrEmpty(recipientEmail)){
            	throw new Exception("Recipient mail is null");
            }
            message.setRecipients(Message.RecipientType.TO, getRecipientsAddressEmail(recipientEmail));
            
            if (!StringUtils.isNullOrEmpty(ccEmail)){
            	message.setRecipients(Message.RecipientType.CC, getRecipientsAddressEmail(ccEmail));
            }
            message.setSubject(title);
            
            MimeMultipart multipart = new MimeMultipart("related");
            BodyPart messageBodyPart = new MimeBodyPart();
            String messageContent = getMessageHTML(title, messagetxt);
            messageBodyPart.setContent(messageContent, "text/html");
            multipart.addBodyPart(messageBodyPart);
            
            message.setContent(multipart);
            message.saveChanges();
            
            transport.connect();
            transport.send(message);

        } catch (IOException e) {
//        	log.info("ERROR: {}", e);
            throw new Exception(e.getMessage());
		}
    }
	
	public Address[] getRecipientsAddressEmail(String recipientEmail){
		String[] recipients = recipientEmail.split(MAIL_SEPARATOR);
		if (recipients.length == 0){
			recipients = recipientEmail.split(MAIL_SEPARATOR_BIS);
		}
		Address[] mailAdress = new Address[recipients.length];
		for (int i = 0; i < recipients.length; i++){
			try {
				mailAdress[i] = new InternetAddress(recipients[i]);
			} catch (AddressException e) {
//				log.info("Address Email Error: {}", recipients[i]);
			}
		}
		return mailAdress;
	}

	private String getMessageHTML(String title, String messagetxt) {
		String messageHTML = "<html><head>"
		        + "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=<?= Yii::$app->charset ?>\" />"
		        + "<title>" + title + "</title>"
		        + "<link href=\"https://fonts.googleapis.com/css?family=Titillium+Web:300\" rel=\"stylesheet\">"
		        + "<style>"
			        + "body{font-family: \'Titillium Web\', sans-serif; font-size: 14px;}"
			        + "a{color: #8f0030;text-decoration: none;}"
			        + ".footer{font-size: 11px;}"
			        + "p{margin-bottom:1em;}"
		        + "</style>"
	        + "</head>"
	        + "<body>"
		        + "<p>Estimado usuario/a,</p>"
		        + "<p>" + messagetxt + "</p>"
		        + "<p>Un cordial saludo,</p>"
		        + "<div class=\"footer\">"
			        + "<p><img src=\"cid:image_logo\" width=\"300\"></p>"
			        + "C/ Mayor, 58, 1&ordm; 28013 Madrid<br/>"
			        + "T. 915471510 Fax. 915400650<br/>"
			        + "<a href=\"https://www.consejogestores.org\">www.consejogestores.org</a>"
		        + "<br><br>"
			    	+ "<p><img src=\"cid:image_planeta\"/><span style=\"color: #A7DF66;\">Antes de imprimir este correo electr&oacute;nico, piense bien si es necesario hacerlo: El medio ambiente es cuesti&oacute;n de todos.</span></p>"
		        + "</div>"
	        + "</body>"
		+ "</html>";
		
		return messageHTML;

	}
}
