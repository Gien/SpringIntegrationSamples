package org.pbank.san.interactor.service.customer.impl;

import java.util.ArrayList;
import java.util.List;

import org.pbank.san.interactor.dto.CustomerDTO;
import org.pbank.san.interactor.dto.CustomerQueryDTO;
import org.pbank.san.interactor.dto.CustomerUpdateDTO;
import org.pbank.san.interactor.dto.CustomersDTO;
import org.pbank.san.interactor.entities.CustomerEntity;
import org.pbank.san.interactor.repositories.CustomerRepository;
import org.pbank.san.interactor.service.customer.ICustomerService;
import org.pbank.san.interactor.service.mail.IMailService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mysql.jdbc.StringUtils;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class CustomerService implements ICustomerService {
	
	@Autowired
	CustomerRepository customerRepository;
	
	@Autowired
	IMailService mailService;

	private static final String title = "Aprobación tarjeta bancaria";
	private static final String messagetxt = "Se aprueba la creación de una tarjeta bancaria para el cliente con DNI <DNI>";
	
	@Override
	public Void addCustomer(CustomerDTO customer){
		customerRepository.save(getCustomerEntity(new CustomerEntity(), customer));
		return null;
	}
	
	private CustomerEntity getCustomerEntity(CustomerEntity customerEntity, CustomerDTO customer){
		customerEntity.setName(customer.getName());
		customerEntity.setIdentificator(customer.getIdentificator());
		customerEntity.setPhone(customer.getPhone());
		return customerEntity;
	}

	@Override
	public Void putCustomer(CustomerUpdateDTO customer){
		CustomerEntity customerEntity = customerRepository.getOne(customer.getId());
		customerRepository.save(getCustomerEntity(customerEntity, customer));
		return null;
	}
	
	private CustomerEntity getCustomerEntity(CustomerEntity customerEntity, CustomerUpdateDTO customer){
		customerEntity.setId(customer.getId());
		customerEntity.setName(customer.getName());
		customerEntity.setIdentificator(customer.getIdentificator());
		customerEntity.setPhone(customer.getPhone());
		return customerEntity;
	}
	
	@Override
	public CustomersDTO getCustomer(CustomerQueryDTO customer){
		CustomersDTO customers = new CustomersDTO();
		List<CustomerEntity> customersEntity = new ArrayList<CustomerEntity>();
		if (!StringUtils.isNullOrEmpty(customer.getIdentificator()) 
				&& customer.getPhone() != null && !StringUtils.isNullOrEmpty(String.valueOf(customer.getPhone()))){
			customersEntity.addAll(customerRepository.findCustomerEntityByIdentificatorAndPhone(customer.getIdentificator(), customer.getPhone()));
		}else{
			if (!StringUtils.isNullOrEmpty(customer.getIdentificator())){
				customersEntity.add(customerRepository.findCustomerEntityByIdentificator(customer.getIdentificator()));
			}
			if (customer.getPhone() != null && !StringUtils.isNullOrEmpty(String.valueOf(customer.getPhone()))){
				customersEntity.addAll(customerRepository.findCustomerEntityByPhone(customer.getPhone()));
			}
		}
		customers.setCustomers(getCustomersDTO(customersEntity));
		return customers;
	}
	
	private List<CustomerUpdateDTO> getCustomersDTO(List<CustomerEntity> customersEntity){
		List<CustomerUpdateDTO> customers = new ArrayList<CustomerUpdateDTO>();
		for(CustomerEntity customerEntity : customersEntity){
			CustomerUpdateDTO customer = new CustomerUpdateDTO();
			customer.setId(customerEntity.getId());
			customer.setName(customerEntity.getName());
			customer.setIdentificator(customerEntity.getIdentificator());
			customer.setPhone(customerEntity.getPhone());
			customers.add(customer);
		}
		
		return customers;
	}

	@Override
	public Void delCustomer(String identificator) throws Exception{
		customerRepository.delete(getCustomerEntity(identificator));
		return null;
	}

	@Override
	public Void mailCustomer(String identificator, String recipientEmail) throws Exception{
//		log.info("****identificator: {}", identificator);
		CustomerEntity customer = customerRepository.findCustomerEntityByIdentificator(identificator);
		if (customer != null){
			sendMail(customer, recipientEmail);
		}else{
			throw new Exception("Customer not found");
		}
		return null;
	}
	
	private CustomerEntity getCustomerEntity(String identificator) throws Exception{
//		log.info("****identificator: {}", identificator);
		CustomerEntity customer = customerRepository.findCustomerEntityByIdentificator(identificator);
		if (customer == null){
			throw new Exception("Customer not found");
		}
		return customer;
	}
	
	private void sendMail(CustomerEntity customer, String recipientEmail) throws Exception{
		mailService.send(recipientEmail, "", title, messagetxt.replace("<DNI>", customer.getIdentificator()));
	}
}
