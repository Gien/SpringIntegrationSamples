package org.pbank.san.interactor.service.mail;

public interface IMailService {
	void setSmtpPort(String smtpPort);

	void setSmtpHost(String smtpHost);

	void setUserMail(String userMail);

	void setUserPass(String userPass);

	void send(String recipientEmail, String ccEmail, String title, String messagetxt) throws Exception;

}
