package org.pbank.san.interactor.api.impl;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.jboss.logging.Param;
import org.pbank.san.interactor.api.ICustomerController;
import org.pbank.san.interactor.dto.CustomerDTO;
import org.pbank.san.interactor.dto.CustomerQueryDTO;
import org.pbank.san.interactor.dto.CustomerUpdateDTO;
import org.pbank.san.interactor.dto.CustomersDTO;
import org.pbank.san.interactor.dto.MailDTO;
import org.pbank.san.interactor.service.customer.ICustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

//import lombok.extern.slf4j.Slf4j;

@RestController
//@Slf4j
public class CustomerController implements ICustomerController {
	
	@Autowired
	ICustomerService customerService;
	
//	@Autowired
//	HttpHeaders headers;
	
	@Autowired
	HttpServletRequest request;

	@Override public ResponseEntity<Void> addCustomer(@Valid @RequestBody CustomerDTO body) {
//		log.info("addCustomer() - {}", body);
		return new ResponseEntity<Void>(customerService.addCustomer(body), HttpStatus.CREATED);
    }

	@Override public ResponseEntity<Void> putCustomer(@Valid @RequestBody CustomerUpdateDTO body) {
//		log.info("putCustomer() - {}", body);
		return new ResponseEntity<Void>(customerService.putCustomer(body), HttpStatus.OK);
    }

	@Override public ResponseEntity<CustomersDTO> getCustomer(@Valid @RequestBody CustomerQueryDTO body) {
//		log.info("getCustomer() - {}", body);
		return new ResponseEntity<CustomersDTO>(customerService.getCustomer(body), HttpStatus.OK);
    }

	@Override public ResponseEntity<Void> delCustomer(@Valid @RequestBody String identificator) throws Exception {
//		log.info("delCustomer() - {}", identificator);
		return new ResponseEntity<Void>(customerService.delCustomer(identificator), HttpStatus.OK);
    }

	@Override public ResponseEntity<Void> mailCustomer(@Valid @RequestBody MailDTO mail) throws Exception {
//		log.info("mailCustomer() - {}", mail.getIdentificator());
		return new ResponseEntity<Void>(customerService.mailCustomer(mail.getIdentificator(), mail.getMail()), HttpStatus.OK);
    }
	
	@Override public ResponseEntity<String> httpResponse (@Param String param1){
		return new ResponseEntity<String>("Hola "+param1+"!!", HttpStatus.OK);
	}
}
