package org.pbank.san.interactor.dto;

public class CustomerQueryDTO {
	
	private String identificator;
	
	private Integer phone;

	public String getIdentificator() {
		return identificator;
	}

	public void setIdentificator(String identificator) {
		this.identificator = identificator;
	}

	public Integer getPhone() {
		return phone;
	}

	public void setPhone(Integer phone) {
		this.phone = phone;
	}

}
