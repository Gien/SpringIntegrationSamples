package com.santander.integration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.santander.integration.mail.domain.Customer;
import com.santander.integration.mail.gateway.CustomerGateway;

@SpringBootApplication
public class BsnIntegrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(BsnIntegrationApplication.class, args);
		
		Customer customer = new Customer();
        customer.setName("John");
        customer.setDni("00000001A");
        customer.setAge(22);
        customer.setId(21);
        
        final CustomerGateway service = customer.getContext().getBean(CustomerGateway.class);
 
        service.send(customer);
	}
}
