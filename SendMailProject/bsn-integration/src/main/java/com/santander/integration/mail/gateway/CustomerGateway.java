package com.santander.integration.mail.gateway;

import org.springframework.messaging.Message;

import com.santander.integration.mail.domain.Customer;

public interface CustomerGateway {
	
	public Message<?> send(Customer customer);
	
}
