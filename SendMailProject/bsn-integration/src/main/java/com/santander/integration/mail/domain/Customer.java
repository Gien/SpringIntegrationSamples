package com.santander.integration.mail.domain;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Customer {
	private int id;
	private String name;
	private String dni;
	private int age;
	
	ApplicationContext context;
	
	public Customer(){
		context = new ClassPathXmlApplicationContext("classpath:send-mail-smtp-config.xml");
	}
	
	public Customer(int id, String name, int age){
		this.id = id;
		this.name = name;
		this.age = age;
	}
	
	public int getId() {
		return id;
	}
 
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
 
	public void setName(String name) {
		this.name = name;
	}
 
	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public int getAge() {
		return age;
	}
 
	public void setAge(int age) {
		this.age = age;
	}
	
	public String toString(){
		String info = String.format("Customer with id = %d, name = %s, age = %d", id, name, age);
		return info;
	}
	
	public String getMailContent(){
		return "Se aprueba la creaci´ñon de una tarjeta bancaria para el cliente con DNI <"+dni+">";
	}
	
	public ApplicationContext getContext(){
		return context;
	}
}