package com.santander.integration.mail.transform;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.mail.MailMessage;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.messaging.Message;

import com.santander.integration.mail.domain.Customer;

public class TransformationCustomerToMail {
	private String mailTo;
	private String mailFrom;
	private String mailSubject;
	
	private static Log logger = LogFactory.getLog(TransformationCustomerToMail.class);
	
	public String getMailTo() {
		return mailTo;
	}

	public void setMailTo(String mailTo) {
		this.mailTo = mailTo;
	}

	public String getMailFrom() {
		return mailFrom;
	}

	public void setMailFrom(String mailFrom) {
		this.mailFrom = mailFrom;
	}

	public String getMailSubject() {
		return mailSubject;
	}

	public void setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
	}

	public MailMessage inboundTransformerMethodMailling(Message<?> message){
		
		logger.info("Transforming MailPayLoad to confirmation e-mail: " + message.getPayload().toString());
		
		Customer customer = (Customer) message.getPayload();
			      
		MailMessage msg = new SimpleMailMessage();

		msg.setTo("gnava@serbatic.es");
		msg.setFrom(mailFrom);
		msg.setSubject(mailSubject);
		msg.setSentDate(new Date());
		msg.setText(customer.getMailContent());
		
		logger.info("Constructed the mail to be sent to the mailbox");
		
		return msg;
	}
}
